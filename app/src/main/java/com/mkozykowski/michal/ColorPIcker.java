package com.mkozykowski.michal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by mkozykowski on 16.08.15.
 */
public class ColorPIcker extends Activity {

    //wszytkie stałe public static final
    public static final String RESULT_COLOR = "resultColor";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.red:
                returnColor(R.color.red);
            break;
            case R.id.blue:
                returnColor(R.color.blue);
            break;
            case R.id.yellow:
                returnColor(R.color.yellow);
            break;
        }
    }

    private void returnColor(int color) {
        Intent intent = new Intent();
        intent.putExtra(RESULT_COLOR,getResources().getColor(color));
        setResult(RESULT_OK,intent);
        finish();
    }
}
