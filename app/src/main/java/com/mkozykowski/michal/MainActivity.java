package com.mkozykowski.michal;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;


public class MainActivity extends ActionBarActivity {

    private static final int RE_GETTOPCOLOR = 1;
    private static final int RE_GETTOPCOLOR2 = 2;
    Button upButton;
    Button downButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        upButton = (Button) findViewById(R.id.button);
        downButton = (Button) findViewById(R.id.button2);
        upButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ColorPIcker.class);
                startActivityForResult(intent, RE_GETTOPCOLOR);

            }
        });
        downButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), ColorPIcker.class);
                startActivityForResult(intent, RE_GETTOPCOLOR2);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == RE_GETTOPCOLOR) {
                int intExtra = data.getIntExtra(ColorPIcker.RESULT_COLOR, 0);
                findViewById(R.id.upLayout).setBackgroundColor(intExtra);
            }
            if (requestCode == RE_GETTOPCOLOR2) {
                int intExtra = data.getIntExtra(ColorPIcker.RESULT_COLOR, 0);
                findViewById(R.id.downLayout).setBackgroundColor(intExtra);
            }

        }
    }
}
