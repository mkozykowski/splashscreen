package com.mkozykowski.michal;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;


/**
 * Created by mkozykowski on 13.08.15.
 */
public class SplashscreenActivity extends Activity {


    private static final String ACTIVITYSTATTIME = "ACTIVITYSTATTIME";
    private static final long SPLASHCLOSETIME = 4000;
    long activityStartTime;

    Handler handler = new Handler();
    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //startActivity(new Intent(getBaseContext(),MainActivity.class));
            startMainAndFinish();
        }
    };

    private void startMainAndFinish() {
        startActivity(new Intent(SplashscreenActivity.this, MainActivity.class));
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        findViewById(R.id.splash).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMainAndFinish();
            }
        });

        if (savedInstanceState == null) {
            activityStartTime = System.currentTimeMillis();
        } else {
            activityStartTime = savedInstanceState.getLong(ACTIVITYSTATTIME);
        }

    }


    @Override
    protected void onPause() {
        super.onPause();
        //Remove any pending posts of Runnable r that are in the message queue.
        handler.removeCallbacks(runnable);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putLong(ACTIVITYSTATTIME, activityStartTime);
    }

    @Override
    protected void onResume() {
        super.onResume();
        long restTime = SPLASHCLOSETIME - (System.currentTimeMillis() - activityStartTime);
        if (restTime > 0) {
            //Causes the Runnable r to be added to the message queue, to be run after the specified amount of time elapses.
            handler.postDelayed(runnable, restTime);
        } else {
            startMainAndFinish();
        }
    }
}
